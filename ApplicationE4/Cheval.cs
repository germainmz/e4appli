﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationE4
{
    public class Cheval
    {
        private int? num;
        private string nom, robe, sexe, race, lien;
        private DateTime ? naissance;

        public Cheval()
        {
            this.Num = null;
            this.Nom = "";
            this.Naissance = null;
            this.Robe = "";
            this.Race = "";
            this.Lien = "";
        }

        public Cheval(int? num, string nom, DateTime ? naissance, string robe, string sexe, string race, string lien)
        {
            this.Num = num;
            this.Nom = nom;
            this.Naissance = naissance;
            this.Robe = robe;
            this.Sexe = sexe;
            this.Race = race;
            this.Lien = lien;
        }

        public int? Num { get { return num; } set { num = value; } }
        public string Nom { get { return nom; } set { nom = value; } }
        public DateTime? Naissance { get { return naissance; } set { naissance = value; } }
        public string Robe { get { return robe; } set { robe = value; } }
        public string Sexe { get { return sexe; } set { sexe = value; } }
        public string Race { get { return race; } set { race = value; } }
        public string Lien { get { return lien; } set { lien = value; } }
    }
}
