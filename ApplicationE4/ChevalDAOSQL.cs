﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace ApplicationE4
{
    public class ChevalDAOSQL : IdentifiantsSQL
    {
        private MySqlConnection maCnx;
        private String maChaineCnx;

        #region Constructeur
        public ChevalDAOSQL()
        {
            maChaineCnx = String.Concat("server=", Server, ";port=", Port, ";database=", Nombase, ";uid=", Login, "; pwd=", Mdp, ";");
            maCnx = new MySqlConnection(maChaineCnx);

            // On teste la création de chaque DAO
            try
            {
                maCnx.Open();
                maCnx.Close();
            }
            catch (Exception e)
            {
                // L'application s'arrête si la connexion est ratée
                MessageBox.Show("Connexion à la base de données MySQL échouée !" + "\n\n" + "L'application va se fermer.", "M2L - Centre Équestre", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }

        }
        #endregion

        #region Liste Chevaux
        public List<Cheval> getChevaux()
        {
            List<Cheval> lesChevaux = null;
            MySqlDataAdapter da = new MySqlDataAdapter("SELECT * FROM CE_CHEVAUX", maChaineCnx);
            DataSet ds = new DataSet();
            try
            {
                lesChevaux = new List<Cheval>();
                da.Fill(ds);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    Cheval leCheval = new Cheval();
                    leCheval.Num = Convert.ToInt32(dr["NUM"]);
                    leCheval.Nom = dr["NOM"].ToString();
                    leCheval.Naissance = Convert.ToDateTime(dr["DATENAISSANCE"].ToString());
                    leCheval.Robe = dr["ROBE"].ToString();
                    leCheval.Sexe = dr["SEXE"].ToString();
                    leCheval.Race = dr["RACE"].ToString();
                    leCheval.Lien = dr["LIENIMAGE"].ToString();
                    lesChevaux.Add(leCheval);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            return lesChevaux;
        }
        #endregion

        #region Update Chevaux
        public bool updateCheval(Cheval updateCheval)
        {

            bool success = false;

            try
            {
                maCnx.Open();
                MySqlCommand req = maCnx.CreateCommand();
                req.CommandText = "UPDATE CE_CHEVAUX SET NOM = @nom, DATENAISSANCE = @date,  ROBE = @robe, SEXE = @sexe, RACE = @race, LIENIMAGE = @lien WHERE NUM = @num";
                req.Parameters.AddWithValue("@num", updateCheval.Num);
                req.Parameters.AddWithValue("@nom", updateCheval.Nom);
                req.Parameters.AddWithValue("@date", String.Format("{0:yyyy/MM/dd}", updateCheval.Naissance.Value));
                req.Parameters.AddWithValue("@robe", updateCheval.Robe);
                req.Parameters.AddWithValue("@sexe", updateCheval.Sexe);
                req.Parameters.AddWithValue("@race", updateCheval.Race);
                req.Parameters.AddWithValue("@lien", updateCheval.Lien);
                req.ExecuteNonQuery();
                maCnx.Close();
                success = true;
            }
            catch (Exception e)
            {

                MessageBox.Show(e.Message);
            }

            return success;
        }
        #endregion

        #region Ajout Cheval
        public bool addCheval(Cheval ChevalaAjouter)
        {

            bool success = false;

            try
            {
                maCnx.Open();
                MySqlCommand req = maCnx.CreateCommand();
                req.CommandText = "INSERT INTO CE_CHEVAUX(NOM, DATENAISSANCE, ROBE, SEXE, RACE, LIENIMAGE) VALUES (@nom, @date, @robe, @sexe, @race, @lien)";
                req.Parameters.AddWithValue("@num", ChevalaAjouter.Num);
                req.Parameters.AddWithValue("@nom", ChevalaAjouter.Nom);
                req.Parameters.AddWithValue("@date", String.Format("{0:yyyy/MM/dd}", ChevalaAjouter.Naissance.Value));
                req.Parameters.AddWithValue("@robe", ChevalaAjouter.Robe);
                req.Parameters.AddWithValue("@sexe", ChevalaAjouter.Sexe);
                req.Parameters.AddWithValue("@race", ChevalaAjouter.Race);
                req.Parameters.AddWithValue("@lien", ChevalaAjouter.Lien);
                req.ExecuteNonQuery();
                maCnx.Close();
                success = true;
            }
            catch (Exception e)
            {

                MessageBox.Show(e.Message);
            }

            return success;
        }
        #endregion

        #region Supprimer Cheval
        public bool deleteCheval(Cheval ChevalaSupprimer)
        {

            bool success = false;

            try
            {
                maCnx.Open();
                MySqlCommand req = maCnx.CreateCommand();
                req.CommandText = "DELETE FROM CE_CHEVAUX WHERE NUM = @num";
                req.Parameters.AddWithValue("@num", ChevalaSupprimer.Num);
                req.ExecuteNonQuery();
                maCnx.Close();
                success = true;
            }
            catch (Exception e)
            {

                MessageBox.Show(e.Message);
            }

            return success;
        }
        #endregion

    }
}
