﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace ApplicationE4
{
    public partial class FConsultationChevaux : Form
    {
        static Utilisateur sessionUtilisateur = new Utilisateur();
        private List<Cheval> lesChevaux = new List<Cheval>();
        private int rangCheval = 0;

        public FConsultationChevaux(string mail)
        {
            InitializeComponent();
            UtilisateurDAOSQL DAOSQL = new UtilisateurDAOSQL();
            sessionUtilisateur = DAOSQL.getUtilisateur(mail);
        }


        #region au Chargement
        private void FConsultationChevaux_Load(object sender, EventArgs e)
        {
            ChevalDAOSQL DAOSQL = new ChevalDAOSQL();
            this.lesChevaux = DAOSQL.getChevaux();
            afficherCheval(rangCheval);
        }
        #endregion

        #region Fonctions ouvertures de fenêtres
        protected void ouvrirFenetreMenu(object obj)
        {
            FMenuPrinc FMenu = new FMenuPrinc(sessionUtilisateur.Mail);
            Application.Run(FMenu);
        }

        private void BtnRetour_Click(object sender, EventArgs e)
        {
            this.Close();
            Thread th = new Thread(ouvrirFenetreMenu);
            th.SetApartmentState(ApartmentState.STA);
            th.Start();
        }
        #endregion

        private void afficherCheval(int rang)
        {
            LABNom.Text = "Nom : " + lesChevaux[rang].Nom;
            LABRace.Text = "Race : " + lesChevaux[rang].Race;
            LABRobe.Text = "Robe : " + lesChevaux[rang].Robe;
            LABSexe.Text = "Sexe : " + lesChevaux[rang].Sexe;
            LABDateNaissance.Text = "Date de naissance : " + lesChevaux[rang].Naissance.Value.ToLongDateString();
            PictureCheval.ImageLocation = lesChevaux[rang].Lien;
            LABPosition.Text = (rang + 1) + "/" + lesChevaux.Count; 
        }

        private void BTNPrec_Click(object sender, EventArgs e)
        {
            if (rangCheval > 0)
            {
                rangCheval--;
                afficherCheval(rangCheval);
            }
            else
            {
                rangCheval = lesChevaux.Count - 1;
                afficherCheval(rangCheval);
            }
        }

        private void BTNSuiv_Click(object sender, EventArgs e)
        {
            if (rangCheval < (lesChevaux.Count -1))
            {
                rangCheval++;
                afficherCheval(rangCheval);
            }
            else
            {
                rangCheval = 0;
                afficherCheval(rangCheval);
            }
        }

        private void BTNPremier_Click(object sender, EventArgs e)
        {
            rangCheval = 0;
            afficherCheval(rangCheval);
        }

        private void BTNDernier_Click(object sender, EventArgs e)
        {
            rangCheval = lesChevaux.Count - 1;
            afficherCheval(rangCheval);
        }
    }
}
