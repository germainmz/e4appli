﻿namespace ApplicationE4
{
    partial class FParametres
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FParametres));
            this.panelInscription = new System.Windows.Forms.Panel();
            this.labTitreInscription = new System.Windows.Forms.Label();
            this.btnParamRetour = new System.Windows.Forms.Button();
            this.btnParamEnregistrer = new System.Windows.Forms.Button();
            this.labInscRole = new System.Windows.Forms.Label();
            this.cbInscRole = new System.Windows.Forms.ComboBox();
            this.labParamErreurPassConfirm = new System.Windows.Forms.Label();
            this.tbParamPassConfirm = new System.Windows.Forms.TextBox();
            this.labParamPassConfirm = new System.Windows.Forms.Label();
            this.labParamErreurPass = new System.Windows.Forms.Label();
            this.tbParamPass = new System.Windows.Forms.TextBox();
            this.labParamPass = new System.Windows.Forms.Label();
            this.labErreurInscEmail = new System.Windows.Forms.Label();
            this.tbEmailConnexion = new System.Windows.Forms.TextBox();
            this.labSousTitreInscription = new System.Windows.Forms.Label();
            this.labInscEmailConnexion = new System.Windows.Forms.Label();
            this.panelInscription.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelInscription
            // 
            this.panelInscription.Controls.Add(this.labTitreInscription);
            this.panelInscription.Controls.Add(this.btnParamRetour);
            this.panelInscription.Controls.Add(this.btnParamEnregistrer);
            this.panelInscription.Controls.Add(this.labInscRole);
            this.panelInscription.Controls.Add(this.cbInscRole);
            this.panelInscription.Controls.Add(this.labParamErreurPassConfirm);
            this.panelInscription.Controls.Add(this.tbParamPassConfirm);
            this.panelInscription.Controls.Add(this.labParamPassConfirm);
            this.panelInscription.Controls.Add(this.labParamErreurPass);
            this.panelInscription.Controls.Add(this.tbParamPass);
            this.panelInscription.Controls.Add(this.labParamPass);
            this.panelInscription.Controls.Add(this.labErreurInscEmail);
            this.panelInscription.Controls.Add(this.tbEmailConnexion);
            this.panelInscription.Controls.Add(this.labSousTitreInscription);
            this.panelInscription.Controls.Add(this.labInscEmailConnexion);
            this.panelInscription.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelInscription.Location = new System.Drawing.Point(0, 0);
            this.panelInscription.Name = "panelInscription";
            this.panelInscription.Size = new System.Drawing.Size(364, 266);
            this.panelInscription.TabIndex = 0;
            // 
            // labTitreInscription
            // 
            this.labTitreInscription.AutoSize = true;
            this.labTitreInscription.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.labTitreInscription.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.labTitreInscription.Location = new System.Drawing.Point(12, 10);
            this.labTitreInscription.Name = "labTitreInscription";
            this.labTitreInscription.Size = new System.Drawing.Size(285, 20);
            this.labTitreInscription.TabIndex = 23;
            this.labTitreInscription.Text = "M2L - Application Centre Équestre";
            // 
            // btnParamRetour
            // 
            this.btnParamRetour.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnParamRetour.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnParamRetour.Location = new System.Drawing.Point(230, 210);
            this.btnParamRetour.Name = "btnParamRetour";
            this.btnParamRetour.Size = new System.Drawing.Size(115, 23);
            this.btnParamRetour.TabIndex = 6;
            this.btnParamRetour.Text = "Retour";
            this.btnParamRetour.UseVisualStyleBackColor = true;
            this.btnParamRetour.Click += new System.EventHandler(this.BtnParamRetour_Click);
            // 
            // btnParamEnregistrer
            // 
            this.btnParamEnregistrer.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnParamEnregistrer.Location = new System.Drawing.Point(230, 150);
            this.btnParamEnregistrer.Name = "btnParamEnregistrer";
            this.btnParamEnregistrer.Size = new System.Drawing.Size(115, 23);
            this.btnParamEnregistrer.TabIndex = 5;
            this.btnParamEnregistrer.Text = "Enregistrer";
            this.btnParamEnregistrer.UseVisualStyleBackColor = true;
            this.btnParamEnregistrer.Click += new System.EventHandler(this.BtnParamEnregistrer_Click);
            // 
            // labInscRole
            // 
            this.labInscRole.AutoSize = true;
            this.labInscRole.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.labInscRole.Location = new System.Drawing.Point(230, 70);
            this.labInscRole.Name = "labInscRole";
            this.labInscRole.Size = new System.Drawing.Size(29, 13);
            this.labInscRole.TabIndex = 22;
            this.labInscRole.Text = "Rôle";
            // 
            // cbInscRole
            // 
            this.cbInscRole.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbInscRole.FormattingEnabled = true;
            this.cbInscRole.Items.AddRange(new object[] {
            "Administrateur",
            "Consultant"});
            this.cbInscRole.Location = new System.Drawing.Point(230, 90);
            this.cbInscRole.MaxDropDownItems = 2;
            this.cbInscRole.Name = "cbInscRole";
            this.cbInscRole.Size = new System.Drawing.Size(115, 21);
            this.cbInscRole.TabIndex = 4;
            // 
            // labParamErreurPassConfirm
            // 
            this.labParamErreurPassConfirm.AutoSize = true;
            this.labParamErreurPassConfirm.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.labParamErreurPassConfirm.Location = new System.Drawing.Point(12, 230);
            this.labParamErreurPassConfirm.Name = "labParamErreurPassConfirm";
            this.labParamErreurPassConfirm.Size = new System.Drawing.Size(0, 13);
            this.labParamErreurPassConfirm.TabIndex = 20;
            // 
            // tbParamPassConfirm
            // 
            this.tbParamPassConfirm.Location = new System.Drawing.Point(12, 210);
            this.tbParamPassConfirm.Name = "tbParamPassConfirm";
            this.tbParamPassConfirm.PasswordChar = '●';
            this.tbParamPassConfirm.Size = new System.Drawing.Size(188, 20);
            this.tbParamPassConfirm.TabIndex = 3;
            // 
            // labParamPassConfirm
            // 
            this.labParamPassConfirm.AutoSize = true;
            this.labParamPassConfirm.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.labParamPassConfirm.Location = new System.Drawing.Point(12, 190);
            this.labParamPassConfirm.Name = "labParamPassConfirm";
            this.labParamPassConfirm.Size = new System.Drawing.Size(130, 13);
            this.labParamPassConfirm.TabIndex = 18;
            this.labParamPassConfirm.Text = "Confirmez le mot de passe";
            // 
            // labParamErreurPass
            // 
            this.labParamErreurPass.AutoSize = true;
            this.labParamErreurPass.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.labParamErreurPass.Location = new System.Drawing.Point(12, 170);
            this.labParamErreurPass.Name = "labParamErreurPass";
            this.labParamErreurPass.Size = new System.Drawing.Size(0, 13);
            this.labParamErreurPass.TabIndex = 17;
            // 
            // tbParamPass
            // 
            this.tbParamPass.Location = new System.Drawing.Point(12, 150);
            this.tbParamPass.Name = "tbParamPass";
            this.tbParamPass.PasswordChar = '●';
            this.tbParamPass.Size = new System.Drawing.Size(188, 20);
            this.tbParamPass.TabIndex = 2;
            // 
            // labParamPass
            // 
            this.labParamPass.AutoSize = true;
            this.labParamPass.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.labParamPass.Location = new System.Drawing.Point(12, 130);
            this.labParamPass.Name = "labParamPass";
            this.labParamPass.Size = new System.Drawing.Size(71, 13);
            this.labParamPass.TabIndex = 15;
            this.labParamPass.Text = "Mot de passe";
            // 
            // labErreurInscEmail
            // 
            this.labErreurInscEmail.AutoSize = true;
            this.labErreurInscEmail.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.labErreurInscEmail.Location = new System.Drawing.Point(12, 110);
            this.labErreurInscEmail.Name = "labErreurInscEmail";
            this.labErreurInscEmail.Size = new System.Drawing.Size(0, 13);
            this.labErreurInscEmail.TabIndex = 14;
            // 
            // tbEmailConnexion
            // 
            this.tbEmailConnexion.Enabled = false;
            this.tbEmailConnexion.Location = new System.Drawing.Point(12, 90);
            this.tbEmailConnexion.Name = "tbEmailConnexion";
            this.tbEmailConnexion.Size = new System.Drawing.Size(188, 20);
            this.tbEmailConnexion.TabIndex = 1;
            // 
            // labSousTitreInscription
            // 
            this.labSousTitreInscription.AutoSize = true;
            this.labSousTitreInscription.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold);
            this.labSousTitreInscription.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.labSousTitreInscription.Location = new System.Drawing.Point(12, 40);
            this.labSousTitreInscription.Name = "labSousTitreInscription";
            this.labSousTitreInscription.Size = new System.Drawing.Size(200, 18);
            this.labSousTitreInscription.TabIndex = 12;
            this.labSousTitreInscription.Text = "Paramètres de la session";
            // 
            // labInscEmailConnexion
            // 
            this.labInscEmailConnexion.AutoSize = true;
            this.labInscEmailConnexion.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.labInscEmailConnexion.Location = new System.Drawing.Point(12, 70);
            this.labInscEmailConnexion.Name = "labInscEmailConnexion";
            this.labInscEmailConnexion.Size = new System.Drawing.Size(99, 13);
            this.labInscEmailConnexion.TabIndex = 11;
            this.labInscEmailConnexion.Text = "Email de connexion";
            // 
            // FParametres
            // 
            this.AcceptButton = this.btnParamEnregistrer;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnParamRetour;
            this.ClientSize = new System.Drawing.Size(364, 266);
            this.Controls.Add(this.panelInscription);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(380, 305);
            this.MinimumSize = new System.Drawing.Size(380, 305);
            this.Name = "FParametres";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Session - M2L Centre Équestre";
            this.Load += new System.EventHandler(this.FParametres_Load);
            this.panelInscription.ResumeLayout(false);
            this.panelInscription.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelInscription;
        private System.Windows.Forms.Button btnParamRetour;
        private System.Windows.Forms.Button btnParamEnregistrer;
        private System.Windows.Forms.Label labInscRole;
        private System.Windows.Forms.ComboBox cbInscRole;
        private System.Windows.Forms.Label labParamErreurPassConfirm;
        private System.Windows.Forms.TextBox tbParamPassConfirm;
        private System.Windows.Forms.Label labParamPassConfirm;
        private System.Windows.Forms.Label labParamErreurPass;
        private System.Windows.Forms.TextBox tbParamPass;
        private System.Windows.Forms.Label labParamPass;
        private System.Windows.Forms.Label labErreurInscEmail;
        private System.Windows.Forms.TextBox tbEmailConnexion;
        private System.Windows.Forms.Label labSousTitreInscription;
        private System.Windows.Forms.Label labInscEmailConnexion;
        private System.Windows.Forms.Label labTitreInscription;
    }
}