﻿namespace ApplicationE4
{
    partial class FConnexion
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FConnexion));
            this.tbEmailConnexion = new System.Windows.Forms.TextBox();
            this.labEmailConnexion = new System.Windows.Forms.Label();
            this.labPass = new System.Windows.Forms.Label();
            this.tbPass = new System.Windows.Forms.TextBox();
            this.panConnexion = new System.Windows.Forms.Panel();
            this.labSousTitreConnexion = new System.Windows.Forms.Label();
            this.labErreurMail = new System.Windows.Forms.Label();
            this.labTitreConnexion = new System.Windows.Forms.Label();
            this.labErreurPass = new System.Windows.Forms.Label();
            this.btnInscription = new System.Windows.Forms.Button();
            this.btnConnexion = new System.Windows.Forms.Button();
            this.panConnexion.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbEmailConnexion
            // 
            this.tbEmailConnexion.Location = new System.Drawing.Point(12, 90);
            this.tbEmailConnexion.Name = "tbEmailConnexion";
            this.tbEmailConnexion.Size = new System.Drawing.Size(188, 20);
            this.tbEmailConnexion.TabIndex = 0;
            // 
            // labEmailConnexion
            // 
            this.labEmailConnexion.AutoSize = true;
            this.labEmailConnexion.Location = new System.Drawing.Point(12, 70);
            this.labEmailConnexion.Name = "labEmailConnexion";
            this.labEmailConnexion.Size = new System.Drawing.Size(99, 13);
            this.labEmailConnexion.TabIndex = 10;
            this.labEmailConnexion.Text = "Email de connexion";
            // 
            // labPass
            // 
            this.labPass.AutoSize = true;
            this.labPass.Location = new System.Drawing.Point(12, 130);
            this.labPass.Name = "labPass";
            this.labPass.Size = new System.Drawing.Size(71, 13);
            this.labPass.TabIndex = 11;
            this.labPass.Text = "Mot de passe";
            // 
            // tbPass
            // 
            this.tbPass.Location = new System.Drawing.Point(12, 150);
            this.tbPass.Name = "tbPass";
            this.tbPass.PasswordChar = '●';
            this.tbPass.Size = new System.Drawing.Size(188, 20);
            this.tbPass.TabIndex = 1;
            // 
            // panConnexion
            // 
            this.panConnexion.Controls.Add(this.labSousTitreConnexion);
            this.panConnexion.Controls.Add(this.labErreurMail);
            this.panConnexion.Controls.Add(this.labTitreConnexion);
            this.panConnexion.Controls.Add(this.labErreurPass);
            this.panConnexion.Controls.Add(this.btnInscription);
            this.panConnexion.Controls.Add(this.btnConnexion);
            this.panConnexion.Controls.Add(this.tbPass);
            this.panConnexion.Controls.Add(this.tbEmailConnexion);
            this.panConnexion.Controls.Add(this.labPass);
            this.panConnexion.Controls.Add(this.labEmailConnexion);
            this.panConnexion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panConnexion.Location = new System.Drawing.Point(0, 0);
            this.panConnexion.Name = "panConnexion";
            this.panConnexion.Size = new System.Drawing.Size(364, 191);
            this.panConnexion.TabIndex = 4;
            // 
            // labSousTitreConnexion
            // 
            this.labSousTitreConnexion.AutoSize = true;
            this.labSousTitreConnexion.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold);
            this.labSousTitreConnexion.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.labSousTitreConnexion.Location = new System.Drawing.Point(12, 40);
            this.labSousTitreConnexion.Name = "labSousTitreConnexion";
            this.labSousTitreConnexion.Size = new System.Drawing.Size(88, 18);
            this.labSousTitreConnexion.TabIndex = 14;
            this.labSousTitreConnexion.Text = "Connexion";
            // 
            // labErreurMail
            // 
            this.labErreurMail.AutoSize = true;
            this.labErreurMail.Location = new System.Drawing.Point(12, 110);
            this.labErreurMail.Name = "labErreurMail";
            this.labErreurMail.Size = new System.Drawing.Size(0, 13);
            this.labErreurMail.TabIndex = 13;
            // 
            // labTitreConnexion
            // 
            this.labTitreConnexion.AutoSize = true;
            this.labTitreConnexion.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labTitreConnexion.Location = new System.Drawing.Point(12, 10);
            this.labTitreConnexion.Name = "labTitreConnexion";
            this.labTitreConnexion.Size = new System.Drawing.Size(285, 20);
            this.labTitreConnexion.TabIndex = 12;
            this.labTitreConnexion.Text = "M2L - Application Centre Équestre";
            // 
            // labErreurPass
            // 
            this.labErreurPass.AutoSize = true;
            this.labErreurPass.Location = new System.Drawing.Point(12, 170);
            this.labErreurPass.Name = "labErreurPass";
            this.labErreurPass.Size = new System.Drawing.Size(0, 13);
            this.labErreurPass.TabIndex = 6;
            // 
            // btnInscription
            // 
            this.btnInscription.Location = new System.Drawing.Point(235, 148);
            this.btnInscription.Name = "btnInscription";
            this.btnInscription.Size = new System.Drawing.Size(115, 23);
            this.btnInscription.TabIndex = 3;
            this.btnInscription.Text = "Inscription";
            this.btnInscription.UseVisualStyleBackColor = true;
            this.btnInscription.Click += new System.EventHandler(this.BtnInscription_Click);
            // 
            // btnConnexion
            // 
            this.btnConnexion.Location = new System.Drawing.Point(235, 88);
            this.btnConnexion.Name = "btnConnexion";
            this.btnConnexion.Size = new System.Drawing.Size(115, 23);
            this.btnConnexion.TabIndex = 2;
            this.btnConnexion.Text = "Connexion";
            this.btnConnexion.UseVisualStyleBackColor = true;
            this.btnConnexion.Click += new System.EventHandler(this.BtnConnexion_Click);
            // 
            // FConnexion
            // 
            this.AcceptButton = this.btnConnexion;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(364, 191);
            this.Controls.Add(this.panConnexion);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FConnexion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Connexion - M2L Centre Équestre";
            this.Load += new System.EventHandler(this.FConnexion_Load);
            this.panConnexion.ResumeLayout(false);
            this.panConnexion.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox tbEmailConnexion;
        private System.Windows.Forms.Label labEmailConnexion;
        private System.Windows.Forms.Label labPass;
        private System.Windows.Forms.TextBox tbPass;
        private System.Windows.Forms.Panel panConnexion;
        private System.Windows.Forms.Button btnInscription;
        private System.Windows.Forms.Button btnConnexion;
        private System.Windows.Forms.Label labErreurPass;
        private System.Windows.Forms.Label labTitreConnexion;
        private System.Windows.Forms.Label labErreurMail;
        private System.Windows.Forms.Label labSousTitreConnexion;
    }
}

