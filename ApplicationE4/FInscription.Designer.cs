﻿namespace ApplicationE4
{
    partial class FInscription
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FInscription));
            this.panelInscription = new System.Windows.Forms.Panel();
            this.btnInscRetour = new System.Windows.Forms.Button();
            this.btnInscInscription = new System.Windows.Forms.Button();
            this.labInscRole = new System.Windows.Forms.Label();
            this.cbInscRole = new System.Windows.Forms.ComboBox();
            this.labInscErreurPassConfirm = new System.Windows.Forms.Label();
            this.tbInscPassConfirm = new System.Windows.Forms.TextBox();
            this.labInscPassConfirm = new System.Windows.Forms.Label();
            this.labInscErreurPass = new System.Windows.Forms.Label();
            this.tbInscPass = new System.Windows.Forms.TextBox();
            this.labInscPass = new System.Windows.Forms.Label();
            this.labErreurInscEmail = new System.Windows.Forms.Label();
            this.tbEmailConnexion = new System.Windows.Forms.TextBox();
            this.labSousTitreInscription = new System.Windows.Forms.Label();
            this.labInscEmailConnexion = new System.Windows.Forms.Label();
            this.labTitreInscription = new System.Windows.Forms.Label();
            this.panelInscription.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelInscription
            // 
            this.panelInscription.Controls.Add(this.btnInscRetour);
            this.panelInscription.Controls.Add(this.btnInscInscription);
            this.panelInscription.Controls.Add(this.labInscRole);
            this.panelInscription.Controls.Add(this.cbInscRole);
            this.panelInscription.Controls.Add(this.labInscErreurPassConfirm);
            this.panelInscription.Controls.Add(this.tbInscPassConfirm);
            this.panelInscription.Controls.Add(this.labInscPassConfirm);
            this.panelInscription.Controls.Add(this.labInscErreurPass);
            this.panelInscription.Controls.Add(this.tbInscPass);
            this.panelInscription.Controls.Add(this.labInscPass);
            this.panelInscription.Controls.Add(this.labErreurInscEmail);
            this.panelInscription.Controls.Add(this.tbEmailConnexion);
            this.panelInscription.Controls.Add(this.labSousTitreInscription);
            this.panelInscription.Controls.Add(this.labInscEmailConnexion);
            resources.ApplyResources(this.panelInscription, "panelInscription");
            this.panelInscription.Name = "panelInscription";
            // 
            // btnInscRetour
            // 
            resources.ApplyResources(this.btnInscRetour, "btnInscRetour");
            this.btnInscRetour.Name = "btnInscRetour";
            this.btnInscRetour.UseVisualStyleBackColor = true;
            this.btnInscRetour.Click += new System.EventHandler(this.BtnInscRetour_Click);
            // 
            // btnInscInscription
            // 
            resources.ApplyResources(this.btnInscInscription, "btnInscInscription");
            this.btnInscInscription.Name = "btnInscInscription";
            this.btnInscInscription.UseVisualStyleBackColor = true;
            this.btnInscInscription.Click += new System.EventHandler(this.BtnInscInscription_Click);
            // 
            // labInscRole
            // 
            resources.ApplyResources(this.labInscRole, "labInscRole");
            this.labInscRole.Name = "labInscRole";
            // 
            // cbInscRole
            // 
            this.cbInscRole.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbInscRole.FormattingEnabled = true;
            this.cbInscRole.Items.AddRange(new object[] {
            resources.GetString("cbInscRole.Items"),
            resources.GetString("cbInscRole.Items1")});
            resources.ApplyResources(this.cbInscRole, "cbInscRole");
            this.cbInscRole.Name = "cbInscRole";
            // 
            // labInscErreurPassConfirm
            // 
            resources.ApplyResources(this.labInscErreurPassConfirm, "labInscErreurPassConfirm");
            this.labInscErreurPassConfirm.Name = "labInscErreurPassConfirm";
            // 
            // tbInscPassConfirm
            // 
            resources.ApplyResources(this.tbInscPassConfirm, "tbInscPassConfirm");
            this.tbInscPassConfirm.Name = "tbInscPassConfirm";
            // 
            // labInscPassConfirm
            // 
            resources.ApplyResources(this.labInscPassConfirm, "labInscPassConfirm");
            this.labInscPassConfirm.Name = "labInscPassConfirm";
            // 
            // labInscErreurPass
            // 
            resources.ApplyResources(this.labInscErreurPass, "labInscErreurPass");
            this.labInscErreurPass.Name = "labInscErreurPass";
            // 
            // tbInscPass
            // 
            resources.ApplyResources(this.tbInscPass, "tbInscPass");
            this.tbInscPass.Name = "tbInscPass";
            // 
            // labInscPass
            // 
            resources.ApplyResources(this.labInscPass, "labInscPass");
            this.labInscPass.Name = "labInscPass";
            // 
            // labErreurInscEmail
            // 
            resources.ApplyResources(this.labErreurInscEmail, "labErreurInscEmail");
            this.labErreurInscEmail.Name = "labErreurInscEmail";
            // 
            // tbEmailConnexion
            // 
            resources.ApplyResources(this.tbEmailConnexion, "tbEmailConnexion");
            this.tbEmailConnexion.Name = "tbEmailConnexion";
            // 
            // labSousTitreInscription
            // 
            resources.ApplyResources(this.labSousTitreInscription, "labSousTitreInscription");
            this.labSousTitreInscription.Name = "labSousTitreInscription";
            // 
            // labInscEmailConnexion
            // 
            resources.ApplyResources(this.labInscEmailConnexion, "labInscEmailConnexion");
            this.labInscEmailConnexion.Name = "labInscEmailConnexion";
            // 
            // labTitreInscription
            // 
            resources.ApplyResources(this.labTitreInscription, "labTitreInscription");
            this.labTitreInscription.Name = "labTitreInscription";
            // 
            // FInscription
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.labTitreInscription);
            this.Controls.Add(this.panelInscription);
            this.MaximizeBox = false;
            this.Name = "FInscription";
            this.Load += new System.EventHandler(this.FInscription_Load);
            this.panelInscription.ResumeLayout(false);
            this.panelInscription.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panelInscription;
        private System.Windows.Forms.Label labTitreInscription;
        private System.Windows.Forms.Label labInscEmailConnexion;
        private System.Windows.Forms.Label labSousTitreInscription;
        private System.Windows.Forms.Label labErreurInscEmail;
        private System.Windows.Forms.TextBox tbEmailConnexion;
        private System.Windows.Forms.Label labInscErreurPass;
        private System.Windows.Forms.TextBox tbInscPass;
        private System.Windows.Forms.Label labInscPass;
        private System.Windows.Forms.Label labInscRole;
        private System.Windows.Forms.ComboBox cbInscRole;
        private System.Windows.Forms.Label labInscErreurPassConfirm;
        private System.Windows.Forms.TextBox tbInscPassConfirm;
        private System.Windows.Forms.Label labInscPassConfirm;
        private System.Windows.Forms.Button btnInscRetour;
        private System.Windows.Forms.Button btnInscInscription;
    }
}