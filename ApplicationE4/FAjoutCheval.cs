﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ApplicationE4
{
    public partial class FAjoutCheval : Form
    {
        public FAjoutCheval()
        {
            InitializeComponent();
        }

        private void BtnAnnuler_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void BtnEnregistrer_Click(object sender, EventArgs e)
        {
            if (VerificationChamps())
            {
                Cheval ChevalaAjouter = new Cheval(null, tbNom.Text.Trim(), dpNaissance.Value, tbRobe.Text.Trim(), tbSexe.Text.Trim(), tbRace.Text.Trim(), tbLien.Text.Trim());

                ChevalDAOSQL DAOSQL = new ChevalDAOSQL();
                DAOSQL.addCheval(ChevalaAjouter);

                MessageBox.Show("Création de l'enregistrement réussie !", "Message");
                Close();
            }
            else
            {

            }
        }

        private bool VerificationChamps()
        {
            bool entreesValide = true;

            string erreurString = "Les champs ";

            if (tbNom.Text == "" || (tbNom.Text.Length) > 25) { entreesValide = false; erreurString += "'Nom' "; }
            if (dpNaissance.Value == null || (dpNaissance.Value) > DateTime.Now) { entreesValide = false; erreurString += "'Nom' "; }
            if (tbLien.Text == "" || (tbLien.Text.Length) > 255) { entreesValide = false; erreurString += "'Lien' "; }
            if (tbRace.Text == "" || (tbRace.Text.Length) > 30) { entreesValide = false; erreurString += "'Race' "; }
            if (tbSexe.Text == "" || (tbSexe.Text.Length) > 30) { entreesValide = false; erreurString += "'Sexe' "; }
            if (tbRobe.Text == "" || (tbRobe.Text.Length) > 30) { entreesValide = false; erreurString += "'Robe' "; }

            erreurString += "ne sont pas valides.";
            if (!entreesValide)
            {
                MessageBox.Show(erreurString, "Erreur");
            }

            return entreesValide;
        }

        private void FAjoutCheval_Load(object sender, EventArgs e)
        {
            tbSexe.Text = "Masculin";
            btnEnregistrer.DialogResult = DialogResult.OK;
        }
    }
}
