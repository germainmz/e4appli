﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace ApplicationE4
{
    public partial class FConnexion : Form
    {
        static string mailSession;

        public FConnexion()
        {
            InitializeComponent();
        }

        public FConnexion(string mail)
        {
            InitializeComponent();
            tbEmailConnexion.Text = mail;
        }

        private void FConnexion_Load(object sender, EventArgs e)
        {
            //On s'assure lors du loading de la fenêtre que la connexion à la BDD est établie
            UtilisateurDAOSQL Test = new UtilisateurDAOSQL();
        }

        protected void ouvrirFenetreInscription(object obj)
        {
            
            FInscription FInsc = new FInscription();
            Application.Run(FInsc);
        }

        protected void ouvrirFenetreConnexion(object obj)
        {
            FMenuPrinc FMenu = new FMenuPrinc(mailSession);
            Application.Run(FMenu);
        }

        #region méthode verif + Connexion
        private void BtnConnexion_Click(object sender, EventArgs e)
        {

            bool entreesValide = verificationChamps();

            if (entreesValide)
            {
                bool mailValide = false;
                bool mdpValide = false;
  
                string mailCherche = "";

                UtilisateurDAOSQL DAOSQLUtilisateur = new UtilisateurDAOSQL();
                List<Utilisateur> toutLesUtilisateurs = DAOSQLUtilisateur.getUtilisateurs();

                //On verifie d'abord si le mail existe dans la bdd
                foreach (Utilisateur unEnregistrement in toutLesUtilisateurs)
                {
                    if(unEnregistrement.Mail.ToLower().Trim() == tbEmailConnexion.Text.ToLower().Trim())
                    {
                        mailValide = true;
                        mailCherche = unEnregistrement.Mail;
                    }
                }

                // Si le mail existe, on regarde si les mdp correspondent
                if (mailValide)
                {
                    if (DAOSQLUtilisateur.checkMdp(mailCherche, tbPass.Text.Trim()))
                    {
                        // Si oui, la connexion est valide
                        mdpValide = true;
                    }
                }
                else
                {
                    // Si le mail n'est pas validé, cela signifie qu'il nest pas enregistré
                    labErreurMail.ForeColor = System.Drawing.Color.Red; labErreurMail.Text = "L'adresse entrée n'est pas enregistrée";
                }

                if(mailValide && !mdpValide)
                {
                    // Si le pass n'est pas validé, cela signifie qu'il est incorrect
                    labErreurPass.ForeColor = System.Drawing.Color.Red; labErreurPass.Text = "Le mot de passe entré est incorrect";
                }

                // Si la connexion est valide, le formulaire se ferme et passe au menu principal avec l'email de l'utilisateur en paramètre
                if (mailValide && mdpValide)
                {
                    mailSession = tbEmailConnexion.Text.Trim();
                    this.Close();
                    Thread th = new Thread(ouvrirFenetreConnexion);
                    th.SetApartmentState(ApartmentState.STA);
                    th.Start();
                }
            }
        }

        #endregion

        private void BtnInscription_Click(object sender, EventArgs e)
        {
            this.Close();
            Thread th = new Thread(ouvrirFenetreInscription);
            th.SetApartmentState(ApartmentState.STA);
            th.Start();
        }

        private bool verificationChamps()
        {
            // Les labels erreurs sont vidés
            labErreurMail.Text = "";
            labErreurPass.Text = "";
            bool OKValide = true;

            // Password vide
            if(tbPass.Text == "") { OKValide = false; labErreurPass.ForeColor = System.Drawing.Color.Red; labErreurPass.Text = "Ce champ ne peut pas être vide"; }
            // Paasword entre 0 et 4 char
            if (tbPass.Text.Length < 4 && tbPass.Text.Length > 0) { OKValide = false; labErreurPass.ForeColor = System.Drawing.Color.Red; labErreurPass.Text = "Le mot de passe est de 4 caractères minimum"; }
            // Paasword dépasse 20 char
            if (tbPass.Text.Length > 20) { OKValide = false; labErreurPass.ForeColor = System.Drawing.Color.Red; labErreurPass.Text = "Le mot de passe est de 20 caractères maximu"; }
            // Email valide
            try { new System.Net.Mail.MailAddress(tbEmailConnexion.Text); }
            catch (ArgumentException) { OKValide = false; labErreurMail.ForeColor = System.Drawing.Color.Red; labErreurMail.Text = "Ce champ ne peut pas être vide"; }
            catch (FormatException) { OKValide = false; labErreurMail.ForeColor = System.Drawing.Color.Red; labErreurMail.Text = "L'adresse entrée est invalide"; }

            return OKValide;
        }
    }
}
