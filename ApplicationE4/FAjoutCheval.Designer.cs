﻿namespace ApplicationE4
{
    partial class FAjoutCheval
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FAjoutCheval));
            this.panAjouter = new System.Windows.Forms.Panel();
            this.btnAnnuler = new System.Windows.Forms.Button();
            this.dpNaissance = new System.Windows.Forms.DateTimePicker();
            this.btnEnregistrer = new System.Windows.Forms.Button();
            this.tbRobe = new System.Windows.Forms.TextBox();
            this.tbRace = new System.Windows.Forms.TextBox();
            this.tbLien = new System.Windows.Forms.TextBox();
            this.LABLien = new System.Windows.Forms.Label();
            this.LABRace = new System.Windows.Forms.Label();
            this.LABSexe = new System.Windows.Forms.Label();
            this.LABRobe = new System.Windows.Forms.Label();
            this.LABDateNaissance = new System.Windows.Forms.Label();
            this.labNom = new System.Windows.Forms.Label();
            this.tbNom = new System.Windows.Forms.TextBox();
            this.tbSexe = new System.Windows.Forms.ComboBox();
            this.panAjouter.SuspendLayout();
            this.SuspendLayout();
            // 
            // panAjouter
            // 
            this.panAjouter.Controls.Add(this.tbSexe);
            this.panAjouter.Controls.Add(this.tbNom);
            this.panAjouter.Controls.Add(this.labNom);
            this.panAjouter.Controls.Add(this.btnAnnuler);
            this.panAjouter.Controls.Add(this.dpNaissance);
            this.panAjouter.Controls.Add(this.btnEnregistrer);
            this.panAjouter.Controls.Add(this.tbRobe);
            this.panAjouter.Controls.Add(this.tbRace);
            this.panAjouter.Controls.Add(this.tbLien);
            this.panAjouter.Controls.Add(this.LABLien);
            this.panAjouter.Controls.Add(this.LABRace);
            this.panAjouter.Controls.Add(this.LABSexe);
            this.panAjouter.Controls.Add(this.LABRobe);
            this.panAjouter.Controls.Add(this.LABDateNaissance);
            this.panAjouter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panAjouter.Location = new System.Drawing.Point(0, 0);
            this.panAjouter.Name = "panAjouter";
            this.panAjouter.Size = new System.Drawing.Size(304, 211);
            this.panAjouter.TabIndex = 0;
            // 
            // btnAnnuler
            // 
            this.btnAnnuler.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnAnnuler.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnAnnuler.Location = new System.Drawing.Point(96, 173);
            this.btnAnnuler.Name = "btnAnnuler";
            this.btnAnnuler.Size = new System.Drawing.Size(91, 23);
            this.btnAnnuler.TabIndex = 7;
            this.btnAnnuler.Text = "Annuler";
            this.btnAnnuler.UseVisualStyleBackColor = true;
            this.btnAnnuler.Click += new System.EventHandler(this.BtnAnnuler_Click);
            // 
            // dpNaissance
            // 
            this.dpNaissance.Location = new System.Drawing.Point(120, 38);
            this.dpNaissance.Name = "dpNaissance";
            this.dpNaissance.Size = new System.Drawing.Size(164, 20);
            this.dpNaissance.TabIndex = 1;
            this.dpNaissance.Value = new System.DateTime(2019, 5, 20, 11, 35, 58, 0);
            // 
            // btnEnregistrer
            // 
            this.btnEnregistrer.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnEnregistrer.Location = new System.Drawing.Point(193, 173);
            this.btnEnregistrer.Name = "btnEnregistrer";
            this.btnEnregistrer.Size = new System.Drawing.Size(91, 23);
            this.btnEnregistrer.TabIndex = 6;
            this.btnEnregistrer.Text = "Enregistrer";
            this.btnEnregistrer.UseVisualStyleBackColor = true;
            this.btnEnregistrer.Click += new System.EventHandler(this.BtnEnregistrer_Click);
            // 
            // tbRobe
            // 
            this.tbRobe.Location = new System.Drawing.Point(55, 63);
            this.tbRobe.MaxLength = 30;
            this.tbRobe.Name = "tbRobe";
            this.tbRobe.Size = new System.Drawing.Size(229, 20);
            this.tbRobe.TabIndex = 2;
            // 
            // tbRace
            // 
            this.tbRace.Location = new System.Drawing.Point(55, 118);
            this.tbRace.MaxLength = 30;
            this.tbRace.Name = "tbRace";
            this.tbRace.Size = new System.Drawing.Size(229, 20);
            this.tbRace.TabIndex = 4;
            // 
            // tbLien
            // 
            this.tbLien.Location = new System.Drawing.Point(101, 147);
            this.tbLien.MaxLength = 255;
            this.tbLien.Name = "tbLien";
            this.tbLien.Size = new System.Drawing.Size(183, 20);
            this.tbLien.TabIndex = 5;
            // 
            // LABLien
            // 
            this.LABLien.AutoSize = true;
            this.LABLien.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.LABLien.Location = new System.Drawing.Point(12, 150);
            this.LABLien.MaximumSize = new System.Drawing.Size(200, 100);
            this.LABLien.Name = "LABLien";
            this.LABLien.Size = new System.Drawing.Size(83, 13);
            this.LABLien.TabIndex = 23;
            this.LABLien.Text = "Lien de l\'image :";
            // 
            // LABRace
            // 
            this.LABRace.AutoSize = true;
            this.LABRace.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.LABRace.Location = new System.Drawing.Point(12, 121);
            this.LABRace.Name = "LABRace";
            this.LABRace.Size = new System.Drawing.Size(39, 13);
            this.LABRace.TabIndex = 21;
            this.LABRace.Text = "Race :";
            // 
            // LABSexe
            // 
            this.LABSexe.AutoSize = true;
            this.LABSexe.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.LABSexe.Location = new System.Drawing.Point(12, 94);
            this.LABSexe.Name = "LABSexe";
            this.LABSexe.Size = new System.Drawing.Size(37, 13);
            this.LABSexe.TabIndex = 19;
            this.LABSexe.Text = "Sexe :";
            // 
            // LABRobe
            // 
            this.LABRobe.AutoSize = true;
            this.LABRobe.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.LABRobe.Location = new System.Drawing.Point(12, 66);
            this.LABRobe.Name = "LABRobe";
            this.LABRobe.Size = new System.Drawing.Size(39, 13);
            this.LABRobe.TabIndex = 17;
            this.LABRobe.Text = "Robe :";
            // 
            // LABDateNaissance
            // 
            this.LABDateNaissance.AutoSize = true;
            this.LABDateNaissance.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.LABDateNaissance.Location = new System.Drawing.Point(12, 40);
            this.LABDateNaissance.Name = "LABDateNaissance";
            this.LABDateNaissance.Size = new System.Drawing.Size(102, 13);
            this.LABDateNaissance.TabIndex = 15;
            this.LABDateNaissance.Text = "Date de naissance :";
            // 
            // labNom
            // 
            this.labNom.AutoSize = true;
            this.labNom.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.labNom.Location = new System.Drawing.Point(12, 14);
            this.labNom.Name = "labNom";
            this.labNom.Size = new System.Drawing.Size(35, 13);
            this.labNom.TabIndex = 26;
            this.labNom.Text = "Nom :";
            // 
            // tbNom
            // 
            this.tbNom.Location = new System.Drawing.Point(55, 11);
            this.tbNom.MaxLength = 30;
            this.tbNom.Name = "tbNom";
            this.tbNom.Size = new System.Drawing.Size(229, 20);
            this.tbNom.TabIndex = 0;
            // 
            // tbSexe
            // 
            this.tbSexe.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tbSexe.FormattingEnabled = true;
            this.tbSexe.Items.AddRange(new object[] {
            "Masculin",
            "Féminin"});
            this.tbSexe.Location = new System.Drawing.Point(55, 91);
            this.tbSexe.MaxDropDownItems = 2;
            this.tbSexe.Name = "tbSexe";
            this.tbSexe.Size = new System.Drawing.Size(229, 21);
            this.tbSexe.TabIndex = 27;
            // 
            // FAjoutCheval
            // 
            this.AcceptButton = this.btnEnregistrer;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnAnnuler;
            this.ClientSize = new System.Drawing.Size(304, 211);
            this.Controls.Add(this.panAjouter);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(320, 250);
            this.MinimumSize = new System.Drawing.Size(320, 250);
            this.Name = "FAjoutCheval";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ajouter un cheval - M2L Centre Équestre";
            this.Load += new System.EventHandler(this.FAjoutCheval_Load);
            this.panAjouter.ResumeLayout(false);
            this.panAjouter.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panAjouter;
        protected System.Windows.Forms.Label labNom;
        protected System.Windows.Forms.Button btnAnnuler;
        private System.Windows.Forms.DateTimePicker dpNaissance;
        protected System.Windows.Forms.Button btnEnregistrer;
        private System.Windows.Forms.TextBox tbRobe;
        private System.Windows.Forms.TextBox tbRace;
        private System.Windows.Forms.TextBox tbLien;
        protected System.Windows.Forms.Label LABLien;
        protected System.Windows.Forms.Label LABRace;
        protected System.Windows.Forms.Label LABSexe;
        protected System.Windows.Forms.Label LABRobe;
        protected System.Windows.Forms.Label LABDateNaissance;
        private System.Windows.Forms.TextBox tbNom;
        private System.Windows.Forms.ComboBox tbSexe;
    }
}