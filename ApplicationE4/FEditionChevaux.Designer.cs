﻿namespace ApplicationE4
{
    partial class FEditionChevaux
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FEditionChevaux));
            this.PanelFull = new System.Windows.Forms.Panel();
            this.dpNaissance = new System.Windows.Forms.DateTimePicker();
            this.tbNom = new System.Windows.Forms.TextBox();
            this.btnEnregistrer = new System.Windows.Forms.Button();
            this.tbRobe = new System.Windows.Forms.TextBox();
            this.tbLien = new System.Windows.Forms.TextBox();
            this.btnRetour = new System.Windows.Forms.Button();
            this.LABPosition = new System.Windows.Forms.Label();
            this.LABLien = new System.Windows.Forms.Label();
            this.BTNSuiv = new System.Windows.Forms.Button();
            this.BTNPremier = new System.Windows.Forms.Button();
            this.BTNDernier = new System.Windows.Forms.Button();
            this.BTNPrec = new System.Windows.Forms.Button();
            this.LABRace = new System.Windows.Forms.Label();
            this.LABSexe = new System.Windows.Forms.Label();
            this.LABRobe = new System.Windows.Forms.Label();
            this.LABDateNaissance = new System.Windows.Forms.Label();
            this.PictureCheval = new System.Windows.Forms.PictureBox();
            this.LABNom = new System.Windows.Forms.Label();
            this.btnSupprimer = new System.Windows.Forms.Button();
            this.btnAjouter = new System.Windows.Forms.Button();
            this.tbSexe = new System.Windows.Forms.ComboBox();
            this.tbRace = new System.Windows.Forms.TextBox();
            this.PanelFull.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureCheval)).BeginInit();
            this.SuspendLayout();
            // 
            // PanelFull
            // 
            this.PanelFull.Controls.Add(this.tbSexe);
            this.PanelFull.Controls.Add(this.btnAjouter);
            this.PanelFull.Controls.Add(this.btnSupprimer);
            this.PanelFull.Controls.Add(this.dpNaissance);
            this.PanelFull.Controls.Add(this.tbNom);
            this.PanelFull.Controls.Add(this.btnEnregistrer);
            this.PanelFull.Controls.Add(this.tbRobe);
            this.PanelFull.Controls.Add(this.tbRace);
            this.PanelFull.Controls.Add(this.tbLien);
            this.PanelFull.Controls.Add(this.btnRetour);
            this.PanelFull.Controls.Add(this.LABPosition);
            this.PanelFull.Controls.Add(this.LABLien);
            this.PanelFull.Controls.Add(this.BTNSuiv);
            this.PanelFull.Controls.Add(this.BTNPremier);
            this.PanelFull.Controls.Add(this.BTNDernier);
            this.PanelFull.Controls.Add(this.BTNPrec);
            this.PanelFull.Controls.Add(this.LABRace);
            this.PanelFull.Controls.Add(this.LABSexe);
            this.PanelFull.Controls.Add(this.LABRobe);
            this.PanelFull.Controls.Add(this.LABDateNaissance);
            this.PanelFull.Controls.Add(this.PictureCheval);
            this.PanelFull.Controls.Add(this.LABNom);
            this.PanelFull.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelFull.Location = new System.Drawing.Point(0, 0);
            this.PanelFull.Name = "PanelFull";
            this.PanelFull.Size = new System.Drawing.Size(584, 361);
            this.PanelFull.TabIndex = 2;
            // 
            // dpNaissance
            // 
            this.dpNaissance.Location = new System.Drawing.Point(408, 30);
            this.dpNaissance.Name = "dpNaissance";
            this.dpNaissance.Size = new System.Drawing.Size(164, 20);
            this.dpNaissance.TabIndex = 1;
            this.dpNaissance.Value = new System.DateTime(2019, 5, 20, 11, 35, 58, 0);
            // 
            // tbNom
            // 
            this.tbNom.Location = new System.Drawing.Point(67, 11);
            this.tbNom.MaxLength = 30;
            this.tbNom.Name = "tbNom";
            this.tbNom.Size = new System.Drawing.Size(227, 20);
            this.tbNom.TabIndex = 0;
            // 
            // btnEnregistrer
            // 
            this.btnEnregistrer.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnEnregistrer.Location = new System.Drawing.Point(481, 165);
            this.btnEnregistrer.Name = "btnEnregistrer";
            this.btnEnregistrer.Size = new System.Drawing.Size(91, 23);
            this.btnEnregistrer.TabIndex = 5;
            this.btnEnregistrer.Text = "Enregistrer";
            this.btnEnregistrer.UseVisualStyleBackColor = true;
            this.btnEnregistrer.Click += new System.EventHandler(this.BtnEnregistrer_Click);
            // 
            // tbRobe
            // 
            this.tbRobe.Location = new System.Drawing.Point(343, 55);
            this.tbRobe.MaxLength = 30;
            this.tbRobe.Name = "tbRobe";
            this.tbRobe.Size = new System.Drawing.Size(229, 20);
            this.tbRobe.TabIndex = 2;
            // 
            // tbLien
            // 
            this.tbLien.Location = new System.Drawing.Point(389, 139);
            this.tbLien.MaxLength = 255;
            this.tbLien.Name = "tbLien";
            this.tbLien.Size = new System.Drawing.Size(183, 20);
            this.tbLien.TabIndex = 4;
            // 
            // btnRetour
            // 
            this.btnRetour.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnRetour.Location = new System.Drawing.Point(391, 271);
            this.btnRetour.Name = "btnRetour";
            this.btnRetour.Size = new System.Drawing.Size(91, 23);
            this.btnRetour.TabIndex = 12;
            this.btnRetour.Text = "Menu Principal";
            this.btnRetour.UseVisualStyleBackColor = true;
            this.btnRetour.Click += new System.EventHandler(this.BtnRetour_Click);
            // 
            // LABPosition
            // 
            this.LABPosition.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.LABPosition.AutoSize = true;
            this.LABPosition.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.LABPosition.Location = new System.Drawing.Point(425, 330);
            this.LABPosition.Name = "LABPosition";
            this.LABPosition.Size = new System.Drawing.Size(24, 13);
            this.LABPosition.TabIndex = 11;
            this.LABPosition.Text = "0/0";
            // 
            // LABLien
            // 
            this.LABLien.AutoSize = true;
            this.LABLien.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.LABLien.Location = new System.Drawing.Point(300, 142);
            this.LABLien.MaximumSize = new System.Drawing.Size(200, 100);
            this.LABLien.Name = "LABLien";
            this.LABLien.Size = new System.Drawing.Size(83, 13);
            this.LABLien.TabIndex = 10;
            this.LABLien.Text = "Lien de l\'image :";
            // 
            // BTNSuiv
            // 
            this.BTNSuiv.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.BTNSuiv.Location = new System.Drawing.Point(497, 326);
            this.BTNSuiv.Name = "BTNSuiv";
            this.BTNSuiv.Size = new System.Drawing.Size(75, 23);
            this.BTNSuiv.TabIndex = 11;
            this.BTNSuiv.Text = "Suivant";
            this.BTNSuiv.UseVisualStyleBackColor = true;
            this.BTNSuiv.Click += new System.EventHandler(this.BTNSuiv_Click);
            // 
            // BTNPremier
            // 
            this.BTNPremier.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.BTNPremier.Location = new System.Drawing.Point(300, 271);
            this.BTNPremier.Name = "BTNPremier";
            this.BTNPremier.Size = new System.Drawing.Size(75, 23);
            this.BTNPremier.TabIndex = 8;
            this.BTNPremier.Text = "Premier";
            this.BTNPremier.UseVisualStyleBackColor = true;
            this.BTNPremier.Click += new System.EventHandler(this.BTNPremier_Click);
            // 
            // BTNDernier
            // 
            this.BTNDernier.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.BTNDernier.Location = new System.Drawing.Point(497, 271);
            this.BTNDernier.Name = "BTNDernier";
            this.BTNDernier.Size = new System.Drawing.Size(75, 23);
            this.BTNDernier.TabIndex = 9;
            this.BTNDernier.Text = "Dernier";
            this.BTNDernier.UseVisualStyleBackColor = true;
            this.BTNDernier.Click += new System.EventHandler(this.BTNDernier_Click);
            // 
            // BTNPrec
            // 
            this.BTNPrec.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.BTNPrec.Location = new System.Drawing.Point(300, 326);
            this.BTNPrec.Name = "BTNPrec";
            this.BTNPrec.Size = new System.Drawing.Size(75, 23);
            this.BTNPrec.TabIndex = 10;
            this.BTNPrec.Text = "Précedent";
            this.BTNPrec.UseVisualStyleBackColor = true;
            this.BTNPrec.Click += new System.EventHandler(this.BTNPrec_Click);
            // 
            // LABRace
            // 
            this.LABRace.AutoSize = true;
            this.LABRace.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.LABRace.Location = new System.Drawing.Point(300, 113);
            this.LABRace.Name = "LABRace";
            this.LABRace.Size = new System.Drawing.Size(39, 13);
            this.LABRace.TabIndex = 5;
            this.LABRace.Text = "Race :";
            // 
            // LABSexe
            // 
            this.LABSexe.AutoSize = true;
            this.LABSexe.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.LABSexe.Location = new System.Drawing.Point(300, 86);
            this.LABSexe.Name = "LABSexe";
            this.LABSexe.Size = new System.Drawing.Size(37, 13);
            this.LABSexe.TabIndex = 4;
            this.LABSexe.Text = "Sexe :";
            // 
            // LABRobe
            // 
            this.LABRobe.AutoSize = true;
            this.LABRobe.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.LABRobe.Location = new System.Drawing.Point(300, 58);
            this.LABRobe.Name = "LABRobe";
            this.LABRobe.Size = new System.Drawing.Size(39, 13);
            this.LABRobe.TabIndex = 3;
            this.LABRobe.Text = "Robe :";
            // 
            // LABDateNaissance
            // 
            this.LABDateNaissance.AutoSize = true;
            this.LABDateNaissance.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.LABDateNaissance.Location = new System.Drawing.Point(300, 32);
            this.LABDateNaissance.Name = "LABDateNaissance";
            this.LABDateNaissance.Size = new System.Drawing.Size(102, 13);
            this.LABDateNaissance.TabIndex = 2;
            this.LABDateNaissance.Text = "Date de naissance :";
            // 
            // PictureCheval
            // 
            this.PictureCheval.Cursor = System.Windows.Forms.Cursors.Default;
            this.PictureCheval.ImageLocation = "";
            this.PictureCheval.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.PictureCheval.InitialImage = null;
            this.PictureCheval.Location = new System.Drawing.Point(16, 37);
            this.PictureCheval.Name = "PictureCheval";
            this.PictureCheval.Size = new System.Drawing.Size(278, 312);
            this.PictureCheval.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PictureCheval.TabIndex = 1;
            this.PictureCheval.TabStop = false;
            // 
            // LABNom
            // 
            this.LABNom.AutoSize = true;
            this.LABNom.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.LABNom.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.LABNom.Location = new System.Drawing.Point(13, 13);
            this.LABNom.Name = "LABNom";
            this.LABNom.Size = new System.Drawing.Size(48, 16);
            this.LABNom.TabIndex = 0;
            this.LABNom.Text = "Nom :";
            // 
            // btnSupprimer
            // 
            this.btnSupprimer.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnSupprimer.Location = new System.Drawing.Point(384, 165);
            this.btnSupprimer.Name = "btnSupprimer";
            this.btnSupprimer.Size = new System.Drawing.Size(91, 23);
            this.btnSupprimer.TabIndex = 6;
            this.btnSupprimer.Text = "Supprimer";
            this.btnSupprimer.UseVisualStyleBackColor = true;
            this.btnSupprimer.Click += new System.EventHandler(this.BtnSupprimer_Click);
            // 
            // btnAjouter
            // 
            this.btnAjouter.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnAjouter.Location = new System.Drawing.Point(481, 194);
            this.btnAjouter.Name = "btnAjouter";
            this.btnAjouter.Size = new System.Drawing.Size(91, 23);
            this.btnAjouter.TabIndex = 7;
            this.btnAjouter.Text = "Ajouter";
            this.btnAjouter.UseVisualStyleBackColor = true;
            this.btnAjouter.Click += new System.EventHandler(this.BtnAjouter_Click);
            // 
            // tbSexe
            // 
            this.tbSexe.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tbSexe.FormattingEnabled = true;
            this.tbSexe.Items.AddRange(new object[] {
            "Masculin",
            "Féminin"});
            this.tbSexe.Location = new System.Drawing.Point(343, 83);
            this.tbSexe.MaxDropDownItems = 2;
            this.tbSexe.Name = "tbSexe";
            this.tbSexe.Size = new System.Drawing.Size(229, 21);
            this.tbSexe.TabIndex = 3;
            // 
            // tbRace
            // 
            this.tbRace.Location = new System.Drawing.Point(343, 110);
            this.tbRace.MaxLength = 4;
            this.tbRace.Name = "tbRace";
            this.tbRace.Size = new System.Drawing.Size(229, 20);
            this.tbRace.TabIndex = 4;
            // 
            // FEditionChevaux
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 361);
            this.Controls.Add(this.PanelFull);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(600, 400);
            this.MinimumSize = new System.Drawing.Size(600, 400);
            this.Name = "FEditionChevaux";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Édition des chevaux - M2L Centre Équestre";
            this.Load += new System.EventHandler(this.FConsultationChevaux_Load);
            this.PanelFull.ResumeLayout(false);
            this.PanelFull.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureCheval)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PanelFull;
        private System.Windows.Forms.TextBox tbRobe;
        private System.Windows.Forms.TextBox tbLien;
        protected System.Windows.Forms.Button btnRetour;
        protected System.Windows.Forms.Label LABPosition;
        protected System.Windows.Forms.Label LABLien;
        protected System.Windows.Forms.Button BTNSuiv;
        protected System.Windows.Forms.Button BTNPremier;
        protected System.Windows.Forms.Button BTNDernier;
        protected System.Windows.Forms.Button BTNPrec;
        protected System.Windows.Forms.Label LABRace;
        protected System.Windows.Forms.Label LABSexe;
        protected System.Windows.Forms.Label LABRobe;
        protected System.Windows.Forms.Label LABDateNaissance;
        protected System.Windows.Forms.PictureBox PictureCheval;
        protected System.Windows.Forms.Label LABNom;
        protected System.Windows.Forms.Button btnEnregistrer;
        private System.Windows.Forms.TextBox tbNom;
        private System.Windows.Forms.DateTimePicker dpNaissance;
        protected System.Windows.Forms.Button btnAjouter;
        protected System.Windows.Forms.Button btnSupprimer;
        private System.Windows.Forms.ComboBox tbSexe;
        private System.Windows.Forms.TextBox tbRace;
    }
}