﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data;
using MySql.Data.MySqlClient;


namespace ApplicationE4
{
    public class UtilisateurDAOSQL : IdentifiantsSQL
    {
        private MySqlConnection maCnx;
        private String maChaineCnx;

        #region Constructeur
        public UtilisateurDAOSQL()
        {
            maChaineCnx = String.Concat("server=", Server, ";port=", Port, ";database=", Nombase,";uid=", Login,"; pwd=", Mdp, ";");
            maCnx = new MySqlConnection(maChaineCnx);

            // On teste la création de chaque DAO
            try
            {
                maCnx.Open();
                maCnx.Close();
            }
            catch (Exception e)
            {
                // L'application s'arrête si la connexion est ratée
                MessageBox.Show("Connexion à la base de données MySQL échouée !" + "\n\n" + "L'application va se fermer.", "M2L - Centre Équestre", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }

        }
        #endregion

        #region Liste Utilisateurs
        public List<Utilisateur> getUtilisateurs()
        {
            List<Utilisateur> lesUtilisateurs = null;
            MySqlDataAdapter da = new MySqlDataAdapter("SELECT * FROM CE_UTILISATEURS", maChaineCnx);
            DataSet ds = new DataSet();
            try
            {
                lesUtilisateurs = new List<Utilisateur>();
                da.Fill(ds);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    Utilisateur leUtilisateur = new Utilisateur();
                    leUtilisateur.Mail = dr["MAIL"].ToString();
                    leUtilisateur.Mdp = dr["PASS"].ToString();
                    leUtilisateur.Role = dr["ROLE"].ToString();
                    lesUtilisateurs.Add(leUtilisateur);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            return lesUtilisateurs;
        }
        #endregion

        #region Get Utilisateur
        public Utilisateur getUtilisateur(string mail)
        {

            List<Utilisateur> lesUtilisateurs = null;
            MySqlDataAdapter da = new MySqlDataAdapter("SELECT * FROM CE_UTILISATEURS WHERE MAIL ='" + mail + "';", maChaineCnx);
            DataSet ds = new DataSet();
            try
            {
                lesUtilisateurs = new List<Utilisateur>();
                da.Fill(ds);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    Utilisateur leUtilisateur = new Utilisateur();
                    leUtilisateur.Mail = dr["MAIL"].ToString();
                    leUtilisateur.Mdp = dr["PASS"].ToString();
                    leUtilisateur.Role = dr["ROLE"].ToString();
                    lesUtilisateurs.Add(leUtilisateur);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }

            Utilisateur resultat = new Utilisateur(lesUtilisateurs[0].Mail, lesUtilisateurs[0].Mdp, lesUtilisateurs[0].Role);

            return resultat;

        }
        #endregion

        #region Ajout Utilisateur
        public bool addUtilisateur(Utilisateur nouveauUser)
        {

            bool success = false;

            try
            {
                maCnx.Open();
                MySqlCommand req = maCnx.CreateCommand();
                req.CommandText = "INSERT INTO CE_UTILISATEURS(MAIL,PASS,ROLE) VALUES(@mail, @pass, @role)";
                req.Parameters.AddWithValue("@mail", nouveauUser.Mail);
                req.Parameters.AddWithValue("@pass", nouveauUser.Mdp);
                req.Parameters.AddWithValue("@role", nouveauUser.Role);
                req.ExecuteNonQuery();
                maCnx.Close();
                success = true;
            }
            catch (Exception e)
            {
                
                MessageBox.Show(e.Message);
            }

            return success;
        }
        #endregion

        #region Update Utilisateur
        public bool updateUtilisateur(Utilisateur updateUtil)
        {

            bool success = false;

            try
            {
                maCnx.Open();
                MySqlCommand req = maCnx.CreateCommand();
                req.CommandText = "UPDATE CE_UTILISATEURS SET PASS = @pass, ROLE = @role WHERE MAIL = @mail";
                req.Parameters.AddWithValue("@mail", updateUtil.Mail);
                req.Parameters.AddWithValue("@pass", updateUtil.Mdp);
                req.Parameters.AddWithValue("@role", updateUtil.Role);
                req.ExecuteNonQuery();
                maCnx.Close();
                success = true;
            }
            catch (Exception e)
            {

                MessageBox.Show(e.Message);
            }

            return success;
        }
        #endregion

        #region Match entre mdp
        public bool checkMdp(string mail, string pass)
        {
            bool mdpValide;
            List<Utilisateur> lesUtilisateurs = null;
            MySqlDataAdapter da = new MySqlDataAdapter("SELECT * FROM CE_UTILISATEURS WHERE MAIL ='" + mail + "';", maChaineCnx);
            DataSet ds = new DataSet();
            try
            {
                lesUtilisateurs = new List<Utilisateur>();
                da.Fill(ds);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    Utilisateur leUtilisateur = new Utilisateur();
                    leUtilisateur.Mail = dr["MAIL"].ToString();
                    leUtilisateur.Mdp = dr["PASS"].ToString();
                    lesUtilisateurs.Add(leUtilisateur);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }

            if (lesUtilisateurs[0].Mdp == pass)
            {
                mdpValide = true;
            }
            else
            {
                mdpValide = false;
            }

            return mdpValide;

        }
        #endregion
    }
}
