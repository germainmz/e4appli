﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace ApplicationE4
{
    public partial class FInscription : Form
    {

        private string mailConnexionEntre = "";

        public FInscription()
        {
            InitializeComponent();
        }


        #region Boutons sorties

        // Fonction ouvrir fenêtre Connexion
        protected void ouvrirFenetreConnexion()
        {
            FConnexion FCnx = new FConnexion(this.mailConnexionEntre);
            Application.Run(FCnx);
        }

        private void BtnInscRetour_Click(object sender, EventArgs e)
        {
            this.Close();
            Thread th = new Thread(ouvrirFenetreConnexion);
            th.SetApartmentState(ApartmentState.STA);
            th.Start();
        }

        // Bouton S'inscrire (clic)
        private void BtnInscInscription_Click(object sender, EventArgs e)
        {

            bool entreesValide = verificationChamps();

            if (entreesValide)
            {
                bool mailValide = true;
                bool mdpValide = false;

                UtilisateurDAOSQL DAOSQLUtilisateur = new UtilisateurDAOSQL();
                List<Utilisateur> toutLesUtilisateurs = DAOSQLUtilisateur.getUtilisateurs();

                //On verifie d'abord si le mail existe dans la bdd
                foreach (Utilisateur unEnregistrement in toutLesUtilisateurs)
                {
                    if (unEnregistrement.Mail.ToLower().Trim() == tbEmailConnexion.Text.ToLower().Trim())
                    {
                        // Si oui, le mail est invalide
                        mailValide = false;
                    }
                }
                // Si le mail n'existe, on regarde si les mdp correspondent
                if (mailValide)
                {
                    if (tbInscPass.Text == tbInscPassConfirm.Text)
                    {
                        // Si oui, l'inscription est valide
                        mdpValide = true;
                    }
                    else
                    {
                        labInscErreurPassConfirm.ForeColor = System.Drawing.Color.Red;
                        labInscErreurPassConfirm.Text = "Les mots de passe ne correspondent pas";
                    }
                }
                else
                {
                    // Si le mail n'est pas validé, cela signifie qu'il nest pas enregistré
                    labErreurInscEmail.ForeColor = System.Drawing.Color.Red; labErreurInscEmail.Text = "L'adresse entrée est déjà utilisée";
                }

                if (mailValide && mdpValide)
                {
                    Utilisateur nouveauUtilisateur = new Utilisateur(tbEmailConnexion.Text.Trim(), tbInscPass.Text, cbInscRole.Text);

                    if (DAOSQLUtilisateur.addUtilisateur(nouveauUtilisateur))
                    {
                        // Affichage de la fenêtre message avec bouton OK
                        string titre = "Message";
                        string message = "Inscription réussie !";
                        MessageBoxButtons boutonOK = MessageBoxButtons.OK;
                        DialogResult reponse;
                        reponse = MessageBox.Show(message, titre, boutonOK);
                        if (reponse == System.Windows.Forms.DialogResult.OK)
                        {
                            // Si appui OK, on lance la fenêtre connexion
                            this.Close();
                            Thread th = new Thread(ouvrirFenetreConnexion);
                            th.SetApartmentState(ApartmentState.STA);
                            th.Start();
                        }
                    }
                }
            }
        }

        #endregion

        #region Chargement

        // Au chargement, la comboBox prend la valeur Administrateur
        private void FInscription_Load(object sender, EventArgs e)
        {
            cbInscRole.SelectedIndex = 0;
        }

        #endregion
        
        // Contrôle des entrées
        private bool verificationChamps()
        {
            // Les labels erreurs sont vidés
            labErreurInscEmail.Text = "";
            labInscErreurPass.Text = "";
            labInscErreurPassConfirm.Text = "";
            bool OKValide = true;

            // Password vide
            if (tbInscPass.Text == "") { OKValide = false; labInscErreurPass.ForeColor = System.Drawing.Color.Red; labInscErreurPass.Text = "Ce champ ne peut pas être vide"; }
            // Paasword entre 0 et 4 char
            if (tbInscPass.Text.Length < 4 && tbInscPass.Text.Length > 0) { OKValide = false; labInscErreurPass.ForeColor = System.Drawing.Color.Red; labInscErreurPass.Text = "Le mot de passe doit être de 4 caractères minimum"; }
            // Paasword dépasse 20 char
            if (tbInscPass.Text.Length > 20) { OKValide = false; labInscErreurPass.ForeColor = System.Drawing.Color.Red; labInscErreurPass.Text = "Le mot de passe est de 20 caractères maximu"; }
            // Password vide
            if (tbInscPassConfirm.Text == "") { OKValide = false; labInscErreurPassConfirm.ForeColor = System.Drawing.Color.Red; labInscErreurPassConfirm.Text = "Ce champ ne peut pas être vide"; }
            // Email valide
            try { new System.Net.Mail.MailAddress(tbEmailConnexion.Text); }
            catch (ArgumentException) { OKValide = false; labErreurInscEmail.ForeColor = System.Drawing.Color.Red; labErreurInscEmail.Text = "Ce champ ne peut pas être vide"; }
            catch (FormatException) { OKValide = false; labErreurInscEmail.ForeColor = System.Drawing.Color.Red; labErreurInscEmail.Text = "L'adresse entrée est invalide"; }

            return OKValide;
        }

    }
}
