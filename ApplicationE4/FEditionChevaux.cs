﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace ApplicationE4
{
    public partial class FEditionChevaux : Form
    {
        static Utilisateur sessionUtilisateur = new Utilisateur();

        private List<Cheval> lesChevaux = new List<Cheval>();
        private int rangCheval = 0;
        private int ? numChevalActif;

        public FEditionChevaux(string mail)
        {
            InitializeComponent();
            UtilisateurDAOSQL DAOSQL = new UtilisateurDAOSQL();
            sessionUtilisateur = DAOSQL.getUtilisateur(mail);
        }


        #region au Chargement
        private void FConsultationChevaux_Load(object sender, EventArgs e)
        {

            if(sessionUtilisateur.Role != "Administrateur")
            {
                DialogResult result = MessageBox.Show("Vous n'êtes pas habilité à effectuer cette action !\n\nL'application va se fermer.", "Erreur", MessageBoxButtons.OK);
                if (result == DialogResult.OK)
                {
                    Application.Exit();
                }
            }

            ChevalDAOSQL DAOSQL = new ChevalDAOSQL();
            this.lesChevaux = DAOSQL.getChevaux();
            afficherCheval(rangCheval);
        }
        #endregion

        #region Fonctions ouvertures de fenêtres
        protected void ouvrirFenetreMenu(object obj)
        {
            FMenuPrinc FMenu = new FMenuPrinc(sessionUtilisateur.Mail);
            Application.Run(FMenu);
        }

        private void BtnRetour_Click(object sender, EventArgs e)
        {
            this.Close();
            Thread th = new Thread(ouvrirFenetreMenu);
            th.SetApartmentState(ApartmentState.STA);
            th.Start();
        }
        #endregion

        private void afficherCheval(int rang)
        {
            numChevalActif = lesChevaux[rang].Num;
            tbNom.Text  = lesChevaux[rang].Nom;
            tbRace.Text = lesChevaux[rang].Race;
            tbSexe.Text = lesChevaux[rang].Sexe;
            tbRobe.Text = lesChevaux[rang].Robe;
            dpNaissance.Value = lesChevaux[rang].Naissance.Value;
            tbLien.Text = lesChevaux[rang].Lien;
            PictureCheval.ImageLocation = lesChevaux[rang].Lien;
            LABPosition.Text = (rang + 1) + "/" + lesChevaux.Count;
        }

        private void BTNPrec_Click(object sender, EventArgs e)
        {
            if (rangCheval > 0)
            {
                rangCheval--;
                afficherCheval(rangCheval);
            }
            else
            {
                rangCheval = lesChevaux.Count -1;
                afficherCheval(rangCheval);
            }
        }

        private void BTNSuiv_Click(object sender, EventArgs e)
        {
            if (rangCheval < (lesChevaux.Count - 1))
            {
                rangCheval++;
                afficherCheval(rangCheval);
            }
            else
            {
                rangCheval = 0;
                afficherCheval(rangCheval);
            }
        }

        private void BTNPremier_Click(object sender, EventArgs e)
        {
            rangCheval = 0;
            afficherCheval(rangCheval);
        }

        private void BTNDernier_Click(object sender, EventArgs e)
        {
            rangCheval = lesChevaux.Count - 1;
            afficherCheval(rangCheval);
        }

        private void BtnEnregistrer_Click(object sender, EventArgs e)
        {
            if (VerificationChamps())
            {
                Cheval updateCheval = new Cheval(numChevalActif, tbNom.Text.Trim(), dpNaissance.Value, tbRobe.Text.Trim(), tbSexe.Text.Trim(), tbRace.Text.Trim(), tbLien.Text.Trim());

                ChevalDAOSQL DAOSQL = new ChevalDAOSQL();
                DAOSQL.updateCheval(updateCheval);
                this.lesChevaux = DAOSQL.getChevaux();
                afficherCheval(rangCheval);
                MessageBox.Show("Modification réussie !", "Message");
            }
        }

        private bool VerificationChamps()
        {
            bool entreesValide = true;

            string erreurString = "Les champs ";

            if (tbNom.Text == "" || (tbNom.Text.Length) > 25) { entreesValide = false; erreurString += "'Nom' "; }
            if (dpNaissance.Value == null || (dpNaissance.Value) > DateTime.Now) { entreesValide = false; erreurString += "'Nom' "; }
            if (tbLien.Text == "" || (tbLien.Text.Length) > 255) { entreesValide = false; erreurString += "'Lien' "; }
            if (tbRace.Text == "" || (tbRace.Text.Length) > 30) { entreesValide = false; erreurString += "'Race' "; }
            if (tbSexe.Text == "" || (tbSexe.Text.Length) > 30) { entreesValide = false; erreurString += "'Sexe' "; }
            if (tbRobe.Text == "" || (tbRobe.Text.Length) > 30) { entreesValide = false; erreurString += "'Robe' "; }

            erreurString += "ne sont pas valides.";
            if (!entreesValide)
            {
                MessageBox.Show(erreurString, "Erreur");
            }

            return entreesValide;
        }

        private void BtnSupprimer_Click(object sender, EventArgs e)
        {
            Cheval ChevalaSupprimer = new Cheval(numChevalActif, tbNom.Text.Trim(), dpNaissance.Value, tbRobe.Text.Trim(), tbSexe.Text.Trim(), tbRace.Text.Trim(), tbLien.Text.Trim());

            ChevalDAOSQL DAOSQL = new ChevalDAOSQL();
            DAOSQL.deleteCheval(ChevalaSupprimer);
            MessageBox.Show("Suppression réussie !", "Message");
            this.lesChevaux = DAOSQL.getChevaux();
            if(rangCheval != 0)
            {
                rangCheval--;
            }
            afficherCheval(rangCheval);
        }

        private void BtnAjouter_Click(object sender, EventArgs e)
        {
            FAjoutCheval ajoutFormCheval = new FAjoutCheval();
            DialogResult reponse = ajoutFormCheval.ShowDialog();
            if (reponse == DialogResult.OK)
            {
                ChevalDAOSQL DAOSQL = new ChevalDAOSQL();
                this.lesChevaux = DAOSQL.getChevaux();
                rangCheval = lesChevaux.Count - 1;
                afficherCheval(rangCheval);
            }
        }
    }
}

