﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationE4
{
    public class Utilisateur
    {
        private string mail, mdp, role;

        public Utilisateur()
        {
            this.Mail   = "";
            this.Mdp    = "";
            this.Role   = "";
        }

        public Utilisateur(string Mail, string Mdp, string Role)
        {
            this.Mail   = Mail;
            this.Mdp    = Mdp;
            this.Role   = Role;
        }

        public string Mail { get { return mail; } set { mail = value; } }
        public string Mdp { get { return mdp; } set { mdp = value; } }
        public string Role { get { return role; } set { role = value; } }
    }
}
