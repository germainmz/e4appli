﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace ApplicationE4
{
    public partial class FMenuPrinc : Form
    {

        static Utilisateur sessionUtilisateur = new Utilisateur();

        public FMenuPrinc(string mail)
        {
            InitializeComponent();
            UtilisateurDAOSQL DAOUtilisateurSQL = new UtilisateurDAOSQL();
            sessionUtilisateur = DAOUtilisateurSQL.getUtilisateur(mail);
        }

        private void FMenuPrinc_Load(object sender, EventArgs e)
        {
            //On s'assure lors du loading de la fenêtre que la connexion à la BDD est établie
            if(sessionUtilisateur.Role != "Administrateur")
            {
                btnVoirEnregistrements.Enabled = false;
            }
        }

        protected void ouvrirFenetreConnexion(object obj)
        {
            FConnexion FCnx = new FConnexion();
            Application.Run(FCnx);
        }

        protected void ouvrirFenetreParametres(object obj)
        {
            FParametres FPara = new FParametres(sessionUtilisateur.Mail);
            Application.Run(FPara);
        }

        protected void ouvrirFenetreConsultation(object obj)
        {
            FConsultationChevaux FCons = new FConsultationChevaux(sessionUtilisateur.Mail);
            Application.Run(FCons);
        }

        protected void ouvrirFenetreEdition(object obj)
        {
            FEditionChevaux FEdit = new FEditionChevaux(sessionUtilisateur.Mail);
            Application.Run(FEdit);
        }

        private void BtnVoirChevaux_Click(object sender, EventArgs e)
        {
            this.Close();
            Thread th = new Thread(ouvrirFenetreConsultation);
            th.SetApartmentState(ApartmentState.STA);
            th.Start();
        }

        private void BtnVoirEnregistrements_Click(object sender, EventArgs e)
        {
            this.Close();
            Thread th = new Thread(ouvrirFenetreEdition);
            th.SetApartmentState(ApartmentState.STA);
            th.Start();
        }

        private void BtnMenuDeconnexion_Click(object sender, EventArgs e)
        {
            this.Close();
            Thread th = new Thread(ouvrirFenetreConnexion);
            th.SetApartmentState(ApartmentState.STA);
            th.Start();
        }

        private void BtnMenuParametres_Click(object sender, EventArgs e)
        {
            this.Close();
            Thread th = new Thread(ouvrirFenetreParametres);
            th.SetApartmentState(ApartmentState.STA);
            th.Start();
        }

    }
}
