﻿namespace ApplicationE4
{
    partial class FMenuPrinc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FMenuPrinc));
            this.labTitreMenuPrinc = new System.Windows.Forms.Label();
            this.labSousTitreMenuPrinc = new System.Windows.Forms.Label();
            this.labMenuConsultation = new System.Windows.Forms.Label();
            this.labMenuEnregistrements = new System.Windows.Forms.Label();
            this.btnVoirChevaux = new System.Windows.Forms.Button();
            this.btnVoirEnregistrements = new System.Windows.Forms.Button();
            this.btnMenuDeconnexion = new System.Windows.Forms.Button();
            this.btnMenuParametres = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labTitreMenuPrinc
            // 
            this.labTitreMenuPrinc.AutoSize = true;
            this.labTitreMenuPrinc.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labTitreMenuPrinc.Location = new System.Drawing.Point(12, 10);
            this.labTitreMenuPrinc.Name = "labTitreMenuPrinc";
            this.labTitreMenuPrinc.Size = new System.Drawing.Size(285, 20);
            this.labTitreMenuPrinc.TabIndex = 13;
            this.labTitreMenuPrinc.Text = "M2L - Application Centre Équestre";
            this.labTitreMenuPrinc.UseMnemonic = false;
            // 
            // labSousTitreMenuPrinc
            // 
            this.labSousTitreMenuPrinc.AutoSize = true;
            this.labSousTitreMenuPrinc.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold);
            this.labSousTitreMenuPrinc.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.labSousTitreMenuPrinc.Location = new System.Drawing.Point(12, 40);
            this.labSousTitreMenuPrinc.Name = "labSousTitreMenuPrinc";
            this.labSousTitreMenuPrinc.Size = new System.Drawing.Size(117, 18);
            this.labSousTitreMenuPrinc.TabIndex = 14;
            this.labSousTitreMenuPrinc.Text = "Menu principal";
            // 
            // labMenuConsultation
            // 
            this.labMenuConsultation.AutoSize = true;
            this.labMenuConsultation.Location = new System.Drawing.Point(52, 90);
            this.labMenuConsultation.Name = "labMenuConsultation";
            this.labMenuConsultation.Size = new System.Drawing.Size(111, 13);
            this.labMenuConsultation.TabIndex = 15;
            this.labMenuConsultation.Text = "Consulter les chevaux";
            this.labMenuConsultation.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labMenuEnregistrements
            // 
            this.labMenuEnregistrements.AutoSize = true;
            this.labMenuEnregistrements.Location = new System.Drawing.Point(190, 90);
            this.labMenuEnregistrements.Name = "labMenuEnregistrements";
            this.labMenuEnregistrements.Size = new System.Drawing.Size(126, 13);
            this.labMenuEnregistrements.TabIndex = 16;
            this.labMenuEnregistrements.Text = "Gérer les enregistrements";
            this.labMenuEnregistrements.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnVoirChevaux
            // 
            this.btnVoirChevaux.Location = new System.Drawing.Point(50, 110);
            this.btnVoirChevaux.Name = "btnVoirChevaux";
            this.btnVoirChevaux.Size = new System.Drawing.Size(115, 23);
            this.btnVoirChevaux.TabIndex = 0;
            this.btnVoirChevaux.Text = "Voir";
            this.btnVoirChevaux.UseVisualStyleBackColor = true;
            this.btnVoirChevaux.Click += new System.EventHandler(this.BtnVoirChevaux_Click);
            // 
            // btnVoirEnregistrements
            // 
            this.btnVoirEnregistrements.Location = new System.Drawing.Point(195, 110);
            this.btnVoirEnregistrements.Name = "btnVoirEnregistrements";
            this.btnVoirEnregistrements.Size = new System.Drawing.Size(115, 23);
            this.btnVoirEnregistrements.TabIndex = 1;
            this.btnVoirEnregistrements.Text = "Voir";
            this.btnVoirEnregistrements.UseVisualStyleBackColor = true;
            this.btnVoirEnregistrements.Click += new System.EventHandler(this.BtnVoirEnregistrements_Click);
            // 
            // btnMenuDeconnexion
            // 
            this.btnMenuDeconnexion.Location = new System.Drawing.Point(268, 156);
            this.btnMenuDeconnexion.Name = "btnMenuDeconnexion";
            this.btnMenuDeconnexion.Size = new System.Drawing.Size(84, 23);
            this.btnMenuDeconnexion.TabIndex = 3;
            this.btnMenuDeconnexion.Text = "Déconnexion";
            this.btnMenuDeconnexion.UseVisualStyleBackColor = true;
            this.btnMenuDeconnexion.Click += new System.EventHandler(this.BtnMenuDeconnexion_Click);
            // 
            // btnMenuParametres
            // 
            this.btnMenuParametres.Location = new System.Drawing.Point(125, 156);
            this.btnMenuParametres.Name = "btnMenuParametres";
            this.btnMenuParametres.Size = new System.Drawing.Size(137, 23);
            this.btnMenuParametres.TabIndex = 2;
            this.btnMenuParametres.Text = "Paramètres de la session";
            this.btnMenuParametres.UseVisualStyleBackColor = true;
            this.btnMenuParametres.Click += new System.EventHandler(this.BtnMenuParametres_Click);
            // 
            // FMenuPrinc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(364, 191);
            this.Controls.Add(this.btnMenuParametres);
            this.Controls.Add(this.btnMenuDeconnexion);
            this.Controls.Add(this.btnVoirEnregistrements);
            this.Controls.Add(this.btnVoirChevaux);
            this.Controls.Add(this.labMenuEnregistrements);
            this.Controls.Add(this.labMenuConsultation);
            this.Controls.Add(this.labSousTitreMenuPrinc);
            this.Controls.Add(this.labTitreMenuPrinc);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(380, 230);
            this.MinimumSize = new System.Drawing.Size(380, 230);
            this.Name = "FMenuPrinc";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Menu Principal - M2L Centre Équestre";
            this.Load += new System.EventHandler(this.FMenuPrinc_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labTitreMenuPrinc;
        private System.Windows.Forms.Label labSousTitreMenuPrinc;
        private System.Windows.Forms.Label labMenuConsultation;
        private System.Windows.Forms.Label labMenuEnregistrements;
        private System.Windows.Forms.Button btnVoirChevaux;
        private System.Windows.Forms.Button btnVoirEnregistrements;
        private System.Windows.Forms.Button btnMenuDeconnexion;
        private System.Windows.Forms.Button btnMenuParametres;
    }
}