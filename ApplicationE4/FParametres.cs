﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace ApplicationE4
{
    public partial class FParametres : Form
    {

        static Utilisateur sessionUtilisateur = new Utilisateur();

        public FParametres(string mail)
        {
            InitializeComponent();
            UtilisateurDAOSQL DAOSQl = new UtilisateurDAOSQL();
            sessionUtilisateur = DAOSQl.getUtilisateur(mail);
        }

        #region au Chargement
        private void FParametres_Load(object sender, EventArgs e)
        {
            tbEmailConnexion.Text = sessionUtilisateur.Mail;
            cbInscRole.Text = sessionUtilisateur.Role;
        }
        #endregion

        #region Méthodes ouvertures de fenêtres
        protected void ouvrirFenetreMenu(object obj)
        {
            FMenuPrinc FMenu = new FMenuPrinc(sessionUtilisateur.Mail);
            Application.Run(FMenu);
        }

        private void BtnParamRetour_Click(object sender, EventArgs e)
        {
            this.Close();
            Thread th = new Thread(ouvrirFenetreMenu);
            th.SetApartmentState(ApartmentState.STA);
            th.Start();
        }
        #endregion

        private void BtnParamEnregistrer_Click(object sender, EventArgs e)
        {
            bool entreesValide = verificationChamps();

            if (entreesValide)
            {
                bool mailValide = false;
                bool mdpValide = false;

                UtilisateurDAOSQL DAOSQLUtilisateur = new UtilisateurDAOSQL();
                List<Utilisateur> toutLesUtilisateurs = DAOSQLUtilisateur.getUtilisateurs();

                //On verifie d'abord si le mail existe dans la bdd
                foreach (Utilisateur unEnregistrement in toutLesUtilisateurs)
                {
                    if (unEnregistrement.Mail.ToLower() == tbEmailConnexion.Text.ToLower())
                    {
                        // Si oui, le mail est invalide
                        mailValide = true;
                    }
                }
                // Si le mail n'existe, on regarde si les mdp correspondent
                if (mailValide)
                {
                    if (tbParamPass.Text == tbParamPassConfirm.Text)
                    {
                        // Si oui, l'inscription est valide
                        mdpValide = true;
                    }
                    else
                    {
                        labParamErreurPassConfirm.ForeColor = System.Drawing.Color.Red;
                        labParamErreurPassConfirm.Text = "Les mots de passe ne correspondent pas";
                    }
                }
                else
                {
                    // Si le mail n'est pas validé, cela signifie qu'il nest pas enregistré
                    labErreurInscEmail.ForeColor = System.Drawing.Color.Red; labErreurInscEmail.Text = "L'adresse entrée est déjà utilisée";
                }

                if (mailValide && mdpValide)
                {
                    Utilisateur UtilisateurUpdate = new Utilisateur(tbEmailConnexion.Text, tbParamPass.Text, cbInscRole.Text);

                    if (DAOSQLUtilisateur.updateUtilisateur(UtilisateurUpdate))
                    {
                        // Affichage de la fenêtre message avec bouton OK
                        string titre = "Message";
                        string message = "Modification réussie !";
                        MessageBoxButtons boutonOK = MessageBoxButtons.OK;
                        DialogResult reponse;
                        reponse = MessageBox.Show(message, titre, boutonOK);
                        if (reponse == System.Windows.Forms.DialogResult.OK)
                        {
                            // Si appui OK, on lance la fenêtre connexion
                            this.Close();
                            Thread th = new Thread(ouvrirFenetreMenu);
                            th.SetApartmentState(ApartmentState.STA);
                            th.Start();
                        }
                    }
                }
            }
        }

        // Contrôle des entrées
        private bool verificationChamps()
        {

            // Les labels erreurs sont vidés
            labErreurInscEmail.Text = "";
            labParamErreurPass.Text = "";
            labParamErreurPass.Text = "";
            bool OKValide = true;

            // Password vide
            if (tbParamPass.Text == "") { OKValide = false; labParamErreurPass.ForeColor = System.Drawing.Color.Red; labParamErreurPass.Text = "Ce champ ne peut pas être vide"; }
            // Paasword entre 0 et 4 char
            if (tbParamPass.Text.Length < 4 && tbParamPass.Text.Length > 0) { OKValide = false; labParamErreurPass.ForeColor = System.Drawing.Color.Red; labParamErreurPass.Text = "Le mot de passe doit être de 4 caractères minimum"; }
            // Paasword dépasse 20 char
            if (tbParamPass.Text.Length > 20) { OKValide = false; labParamErreurPass.ForeColor = System.Drawing.Color.Red; labParamErreurPass.Text = "Le mot de passe est de 20 caractères maximu"; }
            // Password vide
            if (tbParamPassConfirm.Text == "") { OKValide = false; labParamErreurPassConfirm.ForeColor = System.Drawing.Color.Red; labParamErreurPassConfirm.Text = "Ce champ ne peut pas être vide"; }
            // Email valide
            try { new System.Net.Mail.MailAddress(tbEmailConnexion.Text); }
            catch (ArgumentException) { OKValide = false; labErreurInscEmail.ForeColor = System.Drawing.Color.Red; labErreurInscEmail.Text = "Ce champ ne peut pas être vide"; }
            catch (FormatException) { OKValide = false; labErreurInscEmail.ForeColor = System.Drawing.Color.Red; labErreurInscEmail.Text = "L'adresse entrée est invalide"; }

            return OKValide;
        }

    }
}
